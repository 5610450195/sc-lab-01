package model;

public class cashCard {
	private int balance;
	private int deposit;
	private int withdraw;

	public cashCard(int amount) {
		this.balance = amount;
	}

	public void setDeposit(int input) {
		balance += input;
		this.deposit = input;
	}
	
	public void setWithdraw(int output) {
		balance -= output;
		this.withdraw = output;
	}
	
	public String toString() {
		return "balance = "+this.balance+
				"\ndeposit = "+this.deposit+
				"\nwithdraw = "+this.withdraw;
	}

}